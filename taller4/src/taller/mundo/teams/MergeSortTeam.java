 package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }
     
     private static Comparable[] aux;

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
    	 
    	 aux = new Comparable[lista.length];
    	 sortear(lista, 0, lista.length -1, orden);
    	 
    	 
    	 return lista;
     }

     private static Comparable[] merge(Comparable[] lista, int lo, int mid, int hi, TipoOrdenamiento orden)
     {
    	// Trabajo en Clase 
    	 if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
    		 int i = lo;
    		 int j = (mid + 1);
    		 for(int h = lo; h <= hi; h++){
    			 aux[h] = lista[h];
    		 }
    		 
    		 for(int h = lo; h <= hi; h++){
    			 if(i > mid){
    				 lista[h] = aux[j++];
    			 }
    			 else if(j > hi){
    				 lista[h] = aux[i++];
    			 }
    			 else if(less(aux[j], aux[i])){
    				 lista[h] = aux[j++];
    			 }
    			 else{
    				 lista[h] = aux[i++];
    			 }
    		 }
    	 }
    	 else if(orden.equals(TipoOrdenamiento.DESCENDENTE)){
    		 int i = lo;
    		 int j = (mid + 1);
    		 for(int h = lo; h <= hi; h++){
    			 aux[h] = lista[h];
    		 }
    		 
    		 for(int h = lo; h <= hi; h++){
    			 if(i > mid){
    				 lista[h] = aux[j++];
    			 }
    			 else if(j > hi){
    				 lista[h] = aux[i++];
    			 }
    			 else if(max(aux[j], aux[i])){
    				 lista[h] = aux[j++];
    			 }
    			 else{
    				 lista[h] = aux[i++];
    			 }
    		 }
    	 }
    	 
    	 return lista;
     }
     
     private static void sortear(Comparable[] lista, int lo, int hi, TipoOrdenamiento orden){
    	 if(hi <= lo){
    		 return;
    	 }
    	 int mid = lo + (hi-lo)/2;
    	 sortear(lista, lo, mid, orden);
    	 sortear(lista, mid + 1, hi, orden);
    	 merge(lista, lo, mid, hi, orden);
     }
     
     private static boolean less(Comparable v, Comparable w){
    	 return v.compareTo(w) < 0;
     }
     
     private static boolean max(Comparable v, Comparable w){
    	 return v.compareTo(w) >= 0;
     }
     
     


}
